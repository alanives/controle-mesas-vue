import Vue from 'vue';

export function reais(valor) {
    valor = parseFloat(valor);
    let reais = Math.abs(valor).toFixed(2);
    return 'R$ ' + reais.replace('.', ',');
}

Vue.filter('reais', reais);