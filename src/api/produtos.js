const _produtos = [
    { 'id': 1, 'nome': 'Entrada #1', 'valor': 10.00, 'tipo': 'entrada' },
    { 'id': 2, 'nome': 'Entrada #2', 'valor': 8.00, 'tipo': 'entrada' },
    { 'id': 3, 'nome': 'Refrigerante', 'valor': 3.00, 'tipo': 'bebida' },
    { 'id': 4, 'nome': 'Suco', 'valor': 4.00, 'tipo': 'bebida' },
    { 'id': 5, 'nome': 'Prato #1', 'valor': 20.00, 'tipo': 'prato' },
    { 'id': 6, 'nome': 'Prato #2', 'valor': 22.00, 'tipo': 'prato' },
    { 'id': 7, 'nome': 'Sobremesa #1', 'valor': 12.00, 'tipo': 'sobremesa' },
    { 'id': 8, 'nome': 'Sobremesa #2', 'valor': 11.00, 'tipo': 'sobremesa' }
];

export default {

    getProdutos(f) {
        setTimeout(() => f(_produtos), 1000);
    }

}