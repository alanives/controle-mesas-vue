import Vue from 'vue';
import Vuex from 'vuex';
import listaProdutos from '@/api/produtos';

Vue.use(Vuex);

export default new Vuex.Store({

    state: {
        produtos: [],
        mesas: []
    },

    getters: {
        getMesa: state => mesaId => state.mesas[mesaId],

        getPedido: (state, getters) => mesaId => {
            return getters.getMesa(mesaId).pedido;
        },

        getNumeroPessoas: (state, getters) => mesaId => {
            return getters.getMesa(mesaId).numeroPessoas;
        },

        getPagamentos: (state, getters) => mesaId => {
            return getters.getMesa(mesaId).pagamentos;
        },

        getProdutos: state => (filtro) => {
            if (filtro == 'todos') {
                return state.produtos;
            } else {
                return state.produtos.filter(item => item.tipo.match(filtro));
            }
        },

        getTotal: (state, getters) => mesaId => {
            let pedidos = getters.getPedido(mesaId);
            let total = 0;

            pedidos.forEach((element, index, array) => {
                total += parseFloat(element.valor) * parseInt(element.quantidade);
            });

            return total;
        },

        getTotalPago: (state, getters) => mesaId => {
            let pagamentos = getters.getPagamentos(mesaId);
            let totalPago = 0;

            pagamentos.forEach((element, index, array) => {
                totalPago += element;
            });

            return totalPago;
        }
    },

    actions: {
        addMesa(context) {
            context.commit('pushNovaMesa');
        },

        removerMesa(context, index) {
            context.commit('spliceMesa', index);
        },

        fetchProdutos({commit}) {
            return new Promise((resolve, reject) => {
                listaProdutos.getProdutos(produtos => {
                    commit('setProdutos', produtos);
                    resolve();
                });
            });
        },

        addProdutosAoPedido(context, {mesaId, produtos}) {
            context.commit('pushProdutosPedidos', {mesaId, produtos});
        },

        removeProdutoPedido(context, {mesaId, produtoId}) {
            context.commit('splicePedido', {mesaId, produtoId});
        },

        updateProdutoPedido(context, {mesaId, produtoId, quantidade}) {
            context.commit('updatePedido', {mesaId, produtoId, quantidade});
        },

        updateNumeroPessoas(context, {mesaId, numeroPessoas}) {
            context.commit('setNumeroPessoas', {mesaId, numeroPessoas});
        },

        addPagamento(context, {mesaId, valor}) {
            context.commit('pushPagamento', {mesaId, valor});
        },

        removePagamento(context, {mesaId, pagamento}) {
            context.commit('splicePagamento', {mesaId, pagamento});
        }
    },

    mutations: {
        pushNovaMesa(state) {
            state.mesas.push({
                pedido: [],
                numeroPessoas: 1,
                pagamentos: []
            })
        },

        spliceMesa(state, index) {
            state.mesas.splice(index, 1);
        },

        setProdutos(state, produtos) {
            state.produtos = produtos;
        },

        pushProdutosPedidos(state, {mesaId, produtos}) {

            produtos.forEach((element, index, array) => {

                const produtoPedido = state.mesas[mesaId].pedido.find( item => item.id === element.id );

                if (!produtoPedido) {
                    state.mesas[mesaId].pedido.push(element);
                } else {
                    produtoPedido.quantidade += parseInt(element.quantidade);
                }

            });

        },

        splicePedido(state, {mesaId, produtoId}){
            const index = state.mesas[mesaId].pedido.findIndex( item => item.id === produtoId );
            state.mesas[mesaId].pedido.splice(index, 1);
        },

        updatePedido(state, {mesaId, produtoId, quantidade}){
            const produtoPedido = state.mesas[mesaId].pedido.find( item => item.id === produtoId );
            produtoPedido.quantidade = quantidade;
        },

        setNumeroPessoas(state, {mesaId, numeroPessoas}) {
            state.mesas[mesaId].numeroPessoas = numeroPessoas;
        },

        pushPagamento(state, {mesaId, valor}) {
            state.mesas[mesaId].pagamentos.push(parseFloat(valor));
        },

        splicePagamento(state, {mesaId, pagamento}) {
            const pgto = state.mesas[mesaId].pagamentos.findIndex( item => item == pagamento );
            state.mesas[mesaId].pagamentos.splice(pgto, 1)
        }
    }
});