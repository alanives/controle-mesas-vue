import Vue from 'vue';

export default Vue.directive('numero', {
    bind: function(el, binding, vnode) {
        el.addEventListener('keyup', () => {
            let v = el.value;
            v.replace(/[^0-9\.\,]/g, '');
            el.value = v;
        });
    }
});