var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');

//task para o sass
gulp.task('sass', function () {
    return gulp.src('src/assets/scss/style.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('src/assets/css/'));
});

//task para o watch
gulp.task('watch', function () {
    gulp.watch('src/assets/scss/*.scss', ['sass']);
});

//task default gulp
gulp.task('default', ['sass', 'watch']);